﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VEFT_Assignment_1.Models
{

    // Thiss class is not in use anymore.. will be in API.models

    /// <summary>
    /// This class represents a single course, taught in a given semester
    /// </summary>
    public class Course
    {
        /// <summary>
        /// A unique identifier for the course
        /// </summary>
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Name of the course
        /// Example: "Web Services"
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// An identifier for the template of the course.
        /// Example: "T-514-VEFT"
        /// </summary>
        [Required]
        public string TemplateId { get; set; }
        /// <summary>
        /// A date for when the class begins.
        /// Example: "2015-08-17"
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// A date for when the class ends.
        /// Example: "2015-11-08" 
        /// </summary>
        [Required]
        public DateTime EndDate { get; set; }
        /// <summary>
        /// List of all students registered in the class
        /// </summary>
        public List<Student> StudentsInClass { get; set; }
    }
} 