﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Services.Entities
{
    /// <summary>
    /// This class is for waiting list table, which contains a list of persons 
    /// that are currently on a waiting list to be registered in a course.
    /// </summary>
    [Table("CourseWaitingLists")]
    class CourseWaitingLists
    {
        /// <summary>
        /// A database generated identification for a single relation
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// A database generated identification of the course in which the waiting list belongs to.
        /// </summary>
        public int CourseID { get; set; }

        /// <summary>
        /// A database generated identification of the person on the waiting list.
        /// </summary>
        public int PersonID { get; set; }
    }
}
