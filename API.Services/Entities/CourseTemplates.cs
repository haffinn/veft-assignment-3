﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Services.Entities
{
    /// <summary>
    /// This class represents a template of a course, used to store more detailed info for a course.
    /// </summary>
    [Table("CourseTemplates")]
    class CourseTemplates
    {
        /// <summary>
        /// A database-generated identification for a template.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// A template ID for a given course, used to join tables.
        /// Example: "T-514-VEFT"
        /// </summary>
        public string TemplateID { get; set; }

        /// <summary>
        /// A name of the course.
        /// Example: "Vefþjónustur"
        /// </summary>
        public string Name { get; set; }
    }
}
