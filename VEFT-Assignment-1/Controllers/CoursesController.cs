﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models.Courses;
using API.Models.Courses.Students;
using API.Services.Exceptions;
using API.Services.ServiceProviders;

namespace VEFT_Assignment_1.Controllers
{
    /// <summary>
    /// This class manages all functions related to courses and course instances
    /// </summary>
    [RoutePrefix("api/courses")]
    public class CoursesController : ApiController
    {
        private readonly CoursesServiceProvider _service;

        /// <summary>
        /// A constructor
        /// </summary>
        public CoursesController()
        {
            _service = new CoursesServiceProvider();
        }

        /// <summary>
        /// Get list of all courses. If no semester is provided, the "current" semester is used.
        /// </summary>
        /// <param name="semester">Given semester. If no semester is provided, the current semester is used</param>
        /// <returns>A list of all courses</returns>
        [HttpGet]
        [Route("")]
        public List<CourseDTO> GetCourses(string semester = null)
        {
            return _service.GetCoursesBySemester(semester);
        }

        /// <summary>
        /// Get a single course by a given ID.
        /// Returns a more detailed information on the course.
        /// </summary>
        /// <param name="id">Database ID of the course</param>
        /// <returns>A single course with details</returns>
        [HttpGet]
        [Route("{id:int}", Name="GetCourseByID")]
        [ResponseType(typeof(CourseDetailsDTO))]
        public IHttpActionResult GetCourseByID(int id)
        {
            CourseDetailsDTO getCourse;
            try
            {
                getCourse = _service.GetCourseByID(id);
            }
            catch (AppObjectNotFoundException)
            {
                return NotFound();
            }
            return Content(HttpStatusCode.OK, getCourse);
        }

        /// <summary>
        /// Add a new course
        /// </summary>
        /// <param name="model">Info needed for a new course</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(CourseDetailsDTO))]
        public IHttpActionResult AddNewCourse(CourseViewModel model)
        {
            if (ModelState.IsValid)
            { 
                var coursedata = _service.AddNewCourse(model);
                var location = Url.Link("GetCourseByID", new { id = coursedata.ID });
                //return Content(HttpStatusCode.Created, coursedata);
                return Created(location, coursedata);
            }
            return StatusCode(HttpStatusCode.PreconditionFailed);
        }

        /// <summary>
        /// Delete a course.
        /// Also deletes all student registeration info for the given course.
        /// This cannot be undone!
        /// </summary>
        /// <param name="id">ID of the course to be deleted</param>
        /// <returns>1 - if procedure succeeded. Null otherwise</returns>
        [HttpDelete]
        [Route("{id:int}")]
        public IHttpActionResult DeleteCourse(int id)
        {
            int result;
            try
            {
                result = _service.DeleteCourse(id);
            }
            catch (AppObjectNotFoundException)
            {
                return NotFound();
            }

            if (result == 1)
            {
                // Everything is ok
                return StatusCode(HttpStatusCode.NoContent);
            }
            return InternalServerError();
        }

        /// <summary>
        /// Get a list of all students in a given course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>A list of students</returns>
        [HttpGet]
        [Route("{id:int}/students")]
        [ResponseType(typeof (List<StudentDTO>))]
        public IHttpActionResult GetStudentsInCourse (int id)
        {
            List<StudentDTO> content;
            try
            {
                content = _service.GetStudentsInCourse(id);
            }
            catch (AppObjectNotFoundException)
            {
                return NotFound();
            }
            return Content(HttpStatusCode.OK, content);
        }

        /// <summary>
        /// Add a student to a course.
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <param name="model">Info on the student. Requerd: SSN and name.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{id:int}/students")]
        [ResponseType(typeof(StudentDTO))]
        public IHttpActionResult AddStudentToCourse(int id, AddStudentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(HttpStatusCode.PreconditionFailed);
            }
            try
            {
                var result = _service.AddStudentToCourse(id, model);
                return Content(HttpStatusCode.Created, result);
            }
            catch (AppObjectNotFoundException)
            {
                return NotFound();
            }
            catch (AppInternalServerErrorException)
            {
                return InternalServerError();
            }
            catch (OperationCanceledException)
            {
                return StatusCode(HttpStatusCode.PreconditionFailed);
            }
        }

        /// <summary>
        /// Remove a student from a course.
        /// Entry will still exist but student will be marked as inactive.
        /// </summary>
        /// <param name="courseid">ID of the course</param>
        /// <param name="personssn">SSN of the student</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{courseid:int}/students/{personssn}")]
        public IHttpActionResult RemoveStudentFromCourse(int courseid, string personssn)
        {
            // _service.RemoveStudentFromCourse(courseid, personssn);

            if (!ModelState.IsValid)
            {
                return StatusCode(HttpStatusCode.PreconditionFailed);
            }
            try
            {
                _service.RemoveStudentFromCourse(courseid, personssn);
                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (AppObjectNotFoundException)
            {
                return NotFound();
            }
            catch (AppInternalServerErrorException)
            {
                return InternalServerError();
            }
            catch (OperationCanceledException)
            {
                return StatusCode(HttpStatusCode.PreconditionFailed);
            }
        }

        /// <summary>
        /// Update a course instance
        /// </summary>
        /// <param name="id">ID of the course you want to update</param>
        /// <param name="model">CourseUpdateViewModel - info on the course</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id:int}")]
        [ResponseType(typeof(CourseDTO))]
        public IHttpActionResult UpdateCourse(int id, CourseUpdateViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _service.UpdateCourse(id, model);
                    return Ok();
                }
                catch (AppObjectNotFoundException)
                {
                    return NotFound();
                }
                catch (AppInternalServerErrorException)
                {
                    return InternalServerError();
                }
            }
            return StatusCode(HttpStatusCode.PreconditionFailed);
        }

        /// <summary>
        /// Get a list of students on the waitinglist for a given course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>A List of students</returns>
        [HttpGet]
        [Route("{id:int}/waitinglist")]
        [ResponseType(typeof(List<StudentDTO>))]
        public IHttpActionResult GetCourseWaitingList(int id)
        {
            List<StudentDTO> students;
            try
            {
                students = _service.GetCourseWaitingList(id);
            }
            catch (AppObjectNotFoundException)
            {
                return NotFound();
            }
            return Content(HttpStatusCode.OK, students);
        }


        /// <summary>
        /// Add a student to the waiting list
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <param name="model">Info (SSN) for the student</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{id:int}/waitinglist")]
        public IHttpActionResult AddStudentToWaitingList(int id, AddStudentViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = _service.AddStudentToWaitingList(id, model);
                    
                    // Was going to return 201 Created but the waitinglist tester 
                    // is expecting http 200 OK..
                    // return Content(HttpStatusCode.Created, result);

                    return Content(HttpStatusCode.OK, result);
                }
                catch (AppObjectNotFoundException)
                {
                    return NotFound();
                }
                catch (OperationCanceledException)
                {
                    return StatusCode(HttpStatusCode.PreconditionFailed);
                }
            }
            return StatusCode(HttpStatusCode.PreconditionFailed);
        }

    }
}
