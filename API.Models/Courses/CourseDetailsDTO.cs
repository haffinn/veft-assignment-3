﻿using System;
using System.Collections.Generic;
using API.Models.Courses.Students;

namespace API.Models.Courses
{
    /// <summary>
    /// This class represents a single course and contains various 
    /// details about the course.
    /// </summary>
    public class CourseDetailsDTO
    {
        /// <summary>
        /// Database generated unique identifier for the course.
        /// Example: 1337
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The name of the course.
        /// Example: "Web services"
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Template of the course.
        /// Example: "T-514-VEFT"
        /// </summary>
        public string TemplateID { get; set; }

        /// <summary>
        /// The semester in which the course is taught
        /// Example: "20153"
        /// </summary>
        public string Semester { get; set; }

        /// <summary>
        /// Start date of the course. Datetime format, only date portion is used.
        /// Example: "2015-08-15"
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date of the course. Date time format, only date portion is used.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Defines how many students can be enrolled in this course.
        /// </summary>
        public int MaxStudents { get; set; }

        /// <summary>
        /// List of all students in the class
        /// </summary>
        public List<StudentDTO> Students { get; set; }

        /// <summary>
        /// List of all students on waiting list
        /// </summary>
        public List<StudentDTO> Waitinglist { get; set; } 

    }
}
