﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Services.Entities
{
    /// <summary>
    /// This class represents a single student.
    /// </summary>
    [Table("Persons")]
    class Persons
    {
        /// <summary>
        /// A database-generated identification for a single student.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The social security number of the student.
        /// Example: "0101852469"
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Full name of the student.
        /// Example: "Hafþór Snær Þórsson"
        /// </summary>
        public string Name { get; set; }
    }
}
