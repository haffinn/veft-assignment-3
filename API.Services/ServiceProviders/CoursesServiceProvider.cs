﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Models.Courses;
using API.Models.Courses.Students;
using API.Services.Entities;
using API.Services.Exceptions;
using API.Services.Repositories;

namespace API.Services.ServiceProviders
{
    public class CoursesServiceProvider
    {
        private readonly AppDataContext _db;

        public CoursesServiceProvider()
        {
            _db = new AppDataContext();
        }

        /// <summary>
        /// Returns a list of courses belonging to a given semester.
        /// If no semester is provided, the "current" semester will be used.
        /// </summary>
        /// <param name="semester">The given semester</param>
        /// <returns>List of courses</returns>
        public List<CourseDTO> GetCoursesBySemester(string semester = null)
        {
            //1. Validate

            // If semester is not specified - the current semester is used.
            // As of now, the value of current semester is hardcoded for Fall 2015.
            if (string.IsNullOrEmpty(semester))
            {
                semester = "20153";
            }

            //2. Get Courses

            //get student count for each course
            var count =
                from cs in _db.CourseStudents
                group cs by cs.CourseID into grp
                select new
                {
                    ID = grp.Key,
                    studCount = grp.Count()
                };

            var res =
                (from c in _db.Courses
                from ct in _db.CourseTemplates.Where(x => x.TemplateID == c.TemplateID)
                from cs in count.Where(x => x.ID == c.ID)
                where c.Semester == semester
                select new CourseDTO
                {
                    ID = c.ID,
                    Name = ct.Name,
                    StartDate = c.StartDate,
                    StudentCount = cs.studCount
                }).ToList();

            //3. Return
            return res;
        }

        /// <summary>
        /// Get a course instance by ID
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>A detailed info on the course</returns>
        public CourseDetailsDTO GetCourseByID(int id)
        {
            //1. Validate
            var isAvailable = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (isAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Get and assemble the course details
            var course =
               (from c in _db.Courses
                from ct in _db.CourseTemplates.Where(x => x.TemplateID == c.TemplateID)
                where c.ID == id
                select new CourseDetailsDTO
                {
                    ID = c.ID,
                    Name = ct.Name,
                    TemplateID = ct.TemplateID,
                    Semester = c.Semester,
                    StartDate = c.StartDate,
                    EndDate = c.EndDate,
                    MaxStudents = c.MaxStudents
                }).SingleOrDefault();
            if (course != null)
            {
                var students = GetStudentsInCourse(id);
                if (students != null)
                {
                    course.Students = students;
                }
                var waitinglist = GetCourseWaitingList(id);
                if (waitinglist != null)
                {
                    course.Waitinglist = waitinglist;
                }
            }

            //3. Return
            return course;
        }

        /// <summary>
        /// Add a new course to the databse.
        /// </summary>
        /// <param name="model">Info needed for the course</param>
        /// <returns>Detailed info on the new course</returns>
        public CourseDetailsDTO AddNewCourse(CourseViewModel model)
        {
            //1. Validate
            var templateIsAvailable = _db.CourseTemplates.SingleOrDefault
                (x => x.TemplateID == model.TemplateID);
            if (templateIsAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Add the new course to the database
            var newCourse = new Courses
            {
                TemplateID = model.TemplateID,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Semester = model.Semester,
                MaxStudents = model.MaxStudents,
            };

            // StartDate = DateTime.Now,
            // EndDate = DateTime.Now.AddMonths(3),

            _db.Courses.Add(newCourse);
            _db.SaveChanges();

            int newID = newCourse.ID;

            //3. Return
            var myCourse = GetCourseByID(newID);

            return myCourse;
        }

        /// <summary>
        /// Delete a course from the database.
        /// Also removes all student registration info for the course.
        /// This cannot be undone!
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>1 if successfull</returns>
        public int DeleteCourse(int id)
        {
            //1. Validate
            var course = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Delte course
            _db.Courses.Remove(course);


            var students = _db.CourseStudents.Where(x => x.CourseID == id).ToList();
            foreach (var student in students)
            {
                _db.CourseStudents.Remove(student);
            }
            _db.SaveChanges();

            //3. Return
            return 1;

        }

        /// <summary>
        /// Get all students in the given course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>A list of all students in the course</returns>
        public List<StudentDTO> GetStudentsInCourse(int id)
        {
            //1. Validate
            var isAvailable = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (isAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Get students
            var students =
                (from p in _db.Persons
                from cs in _db.CourseStudents.Where(x => x.PersonID == p.ID)
                where (cs.CourseID == id) && (cs.StudentActive) 
                select new StudentDTO
                {
                    Name = p.Name,
                    SSN = p.SSN
                }).ToList();

            //3. Return
            return students;

        }

        /// <summary>
        /// Add a student to a course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <param name="model">Info on the student</param>
        /// <returns>The student info</returns>
        public StudentDTO AddStudentToCourse(int id, AddStudentViewModel model)
        {
            //### 1. Validation

            // Does the course exist?
            var course = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }

            // Does the person exist?
            var person = _db.Persons.SingleOrDefault(x => x.SSN == model.SSN);
            if (person == null)
            {
                throw new AppObjectNotFoundException();
            }
            
            // Has the max student value been reached?
            var registeredStudents = _db.CourseStudents.Count
                (x => (x.CourseID == id) && x.StudentActive);

            if (registeredStudents >= course.MaxStudents)
            {
                // Too many students registered
                throw new OperationCanceledException("Too many students registered");
            }

            //### 2. Add the student to the course

            // Is the student already registered in the course?  
            var studentInstance = _db.CourseStudents.Where
                (x => (x.PersonID == person.ID) && (x.CourseID == course.ID));

            var studentCount = studentInstance.Count();
            if (studentCount > 1)
            {
                // Student is registered more than once.. this should never happen
                throw new AppInternalServerErrorException();
            }

            if (studentCount == 1)
            {
                // Student is registered. Is he/she active?
                var isActive = studentInstance.SingleOrDefault();
                if (isActive == null)
                {
                    // What? Someting is really wrong..
                    // This should never happen.
                    throw new AppInternalServerErrorException();
                }
                if (isActive.StudentActive)
                {
                    // Student is registered and active.
                    // Illigal operation - cannot add student twice to a course
                    throw new OperationCanceledException("Student is already registered to this course");
                }
                // We reach here if student is not active, lets change that.
                isActive.StudentActive = true;
                _db.SaveChanges();
            }
            else if (studentCount == 0)
            {
                // Not registered, add to databse.
                var courseStudent = new CourseStudents
                {
                    PersonID = person.ID,
                    CourseID = course.ID,
                    StudentActive = true
                };

                _db.CourseStudents.Add(courseStudent);
                _db.SaveChanges();
            }

            // Check if student is on the waitinglist
            var waitinglist = _db.CourseWaitingLists.SingleOrDefault
                (x => (x.CourseID == course.ID) && (x.PersonID == person.ID));
            if (waitinglist != null)
            {
                // Student is on the waiting list, remove it
                RemoveFromWaitingList(course.ID, person.ID);
            }

            //### 3. Figure out what to return

            var returnValue = new StudentDTO
            {
                Name = person.Name,
                SSN = person.SSN
            };

            return returnValue;
        }


        /// <summary>
        /// Remove the student from a course.
        /// The databse entry will not be deleted but the student will be marked as inactive.
        /// </summary>
        /// <param name="courseid">ID of the course</param>
        /// <param name="personssn">SSN of the student</param>
        /// <returns>1 if successfull</returns>
        public int RemoveStudentFromCourse(int courseid, string personssn)
        {
            //### 1. Validate
            // Does the course exist?
            var course = _db.Courses.SingleOrDefault(x => x.ID == courseid);
            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }

            // Does the person exist?
            var person = _db.Persons.SingleOrDefault(x => x.SSN == personssn);
            if (person == null)
            {
                throw new AppObjectNotFoundException();
            }

            //### 2. Remove the student (mark as inactive)
            // Is the person registered in the course?
            var studentInstance = _db.CourseStudents.Where
                (x => (x.PersonID == person.ID) && (x.CourseID == course.ID));

            var studentCount = studentInstance.Count();
            if (studentCount > 1)
            {
                // Student is registered more than once.. this should never happen
                throw new AppInternalServerErrorException();
            }
            if (studentCount == 0)
            {
                // Student is not registered in the course
                throw new AppObjectNotFoundException();
            }
            if (studentCount == 1)
            {
                // Student is registered. Is he/she active?
                var isActive = studentInstance.SingleOrDefault();
                if (isActive == null)
                {
                    // What? Someting is really wrong..
                    // This should never happen.
                    throw new AppInternalServerErrorException();
                }
                if (isActive.StudentActive == false)
                {
                    // Student is already inactive (not in the course)
                    // Cannot remove more than once now can we?
                    throw new AppObjectNotFoundException();
                }

                // We reach here if the student is active
                // Lets mark the student as inactive.
                 isActive.StudentActive = false;
                _db.SaveChanges();
            }

            //### 3. Return
            return 1;
        }

        /// <summary>
        /// Update a course instance
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <param name="model">Info to be changed</param>
        /// <returns></returns>
        public CourseDTO UpdateCourse(int id, CourseUpdateViewModel model)
        {
            //1. Validate

            var courseEntity = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (courseEntity == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Update

            courseEntity.StartDate = model.StartDate;
            courseEntity.EndDate = model.EndDate;
            courseEntity.MaxStudents = model.MaxStudents;
            _db.SaveChanges();

            //3. Return
            var courseTemplate = _db.CourseTemplates.SingleOrDefault(x => x.TemplateID == courseEntity.TemplateID);
            if (courseTemplate == null)
            {
                throw new AppInternalServerErrorException();
            }

            var count = _db.CourseStudents.Count(x => x.CourseID == courseEntity.ID);
            var result = new CourseDTO
            {
                ID = courseEntity.ID,
                Name = courseTemplate.Name,
                StartDate = model.StartDate,
                StudentCount = count
            };
            return result;
        }

        /// <summary>
        /// Get all students on the waitinglist for a given course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <returns>A list containing all students on the waitinglist</returns>
        public List<StudentDTO> GetCourseWaitingList(int id)
        {
            //1. Validate
            var isAvailable = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (isAvailable == null)
            {
                throw new AppObjectNotFoundException();
            }

            //2. Get students
            var students =
                (from p in _db.Persons
                 from cs in _db.CourseWaitingLists.Where(x => x.PersonID == p.ID)
                 where cs.CourseID == id
                 select new StudentDTO
                 {
                     Name = p.Name,
                     SSN = p.SSN
                 }).ToList();

            //3. Return
            return students;
        }

        /// <summary>
        /// Add a student to the waiting list for a given course
        /// </summary>
        /// <param name="id">ID of the course</param>
        /// <param name="model">Info on the student</param>
        /// <returns></returns>
        public StudentDTO AddStudentToWaitingList(int id, AddStudentViewModel model)
        {
            //### 1. Validate
            // Does the course exist?
            var course = _db.Courses.SingleOrDefault(x => x.ID == id);
            if (course == null)
            {
                throw new AppObjectNotFoundException();
            }

            // Does the person exist?
            var person = _db.Persons.SingleOrDefault(x => x.SSN == model.SSN);
            if (person == null)
            {
                throw new AppObjectNotFoundException();
            }

            // Is the person already on the waiting list?
            var alreadyOnWaitingList = _db.CourseWaitingLists.Count
                (x => (x.PersonID == person.ID) && (x.CourseID == course.ID));
            if (alreadyOnWaitingList > 0)
            {
                throw new OperationCanceledException("Already on waiting list");
            }

            // Is the person already registered in the class?
            var alreadyRegistered = _db.CourseStudents.Count
                (x => (x.CourseID == course.ID) && (x.PersonID == person.ID) && (x.StudentActive));
            if (alreadyRegistered > 0)
            {
                throw new OperationCanceledException("Already registered in course");
            }

            //### 2. Add the student to the waiting list

            var waitinglist = new CourseWaitingLists
            {
                PersonID = person.ID,
                CourseID = course.ID
            };

            _db.CourseWaitingLists.Add(waitinglist);
            _db.SaveChanges();


            //### 3. Return

            var returnValue = new StudentDTO
            {
                Name = person.Name,
                SSN = person.SSN
            };

            return returnValue;
        }

        /*private bool IsStudentRegisteredAndInactive(int courseid, int personid)
        {
            var studentInstance = _db.CourseStudents.Where
                (x => (x.CourseID == courseid) && (x.PersonID == personid));

            var studentCount = studentInstance.Count();
            if (studentCount > 1)
            {
                // Student is registered more than once.. this should never happen
                throw new AppInternalServerErrorException();
            }
            if (studentCount == 0 || studentCount != 1)
            {
                // Student is not registered in the course
                return false;
            }

            // Student is registered. Is he/she active?
            var isActive = studentInstance.SingleOrDefault();
            if (isActive == null)
            {
                // What? Someting is really wrong..
                // This should never happen.
                throw new AppInternalServerErrorException();
            }
            return isActive.StudentActive;
        }*/

        /*private bool IsStudentRegisteredAndActive(int courseid, string personssn)
        {
            //### 1. Get all students
            var studentsInCourse = GetStudentsInCourse(courseid);

            //### 2. Filter
            var isRegisteredAndActive = studentsInCourse.Count(x => x.SSN == personssn);

            //### 3. Return
            if (isRegisteredAndActive > 1)
            {
                // Is registerd more than once, should never happen
                throw new AppInternalServerErrorException();
            }
            if (isRegisteredAndActive == 1)
            {
                // Registered AND active
                return true;
            }
            return false;
        }*/

        /// <summary>
        /// Remove a student from the waitinglist for a given course.
        /// This is a private function (for now).
        /// </summary>
        /// <param name="courseid">Database ID of the course</param>
        /// <param name="personid">Database ID of the person</param>
        private void RemoveFromWaitingList(int courseid, int personid)
        {
            //1. Validate

            var wlist = _db.CourseWaitingLists.SingleOrDefault
                (x => (x.CourseID == courseid) && (x.PersonID == personid));
            if (wlist == null)
            {
                // Student not on the waiting list.
                throw new AppObjectNotFoundException();
            }

            //2. Delte course
            _db.CourseWaitingLists.Remove(wlist);
            _db.SaveChanges();
        }
    }
}
