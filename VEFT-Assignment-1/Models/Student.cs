﻿
using System.ComponentModel.DataAnnotations;

namespace VEFT_Assignment_1.Models
{
    //This class in not in use anymore - all models moved to API.models


    /// <summary>
    /// This class represents a single student in a course.
    /// </summary>
    public class Student
    {
        /// <summary>
        /// The social security number of the student.
        /// Example: "0101852469"
        /// </summary>
        [Required]
        public string SSN { get; set; }
        /// <summary>
        /// Full name of the student.
        /// Example: "Hafþór Snær Þórsson"
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}