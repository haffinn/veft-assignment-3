﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Services.Entities
{
    /// <summary>
    /// This class represents the database table for students in course.
    /// </summary>
    [Table("CourseStudents")]
    class CourseStudents
    {
        /// <summary>
        /// A database-generated identification for a single connection.
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// Database ID of the course which the student is registered in
        /// </summary>
        public int CourseID { get; set; }
        
        /// <summary>
        /// Database ID of the student registered in a course
        /// </summary>
        public int PersonID { get; set; }

        /// <summary>
        /// A flag to specify if the student is active or not.
        /// Default value is true.
        /// </summary>
        public bool StudentActive { get; set; }
    }
}
