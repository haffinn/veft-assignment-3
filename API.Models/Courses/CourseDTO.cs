﻿using System;

namespace API.Models.Courses
{
    /// <summary>
    /// This class represents an item in a list of courses.
    /// </summary>
    public class CourseDTO
    {
        /// <summary>
        /// Database generated unique identifier for the course.
        /// Example: 1337
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The name of the course.
        /// Example: "Web services"
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The date when the course starts.
        /// The time portion is unused, only dates are used.
        /// Example: "20-08-2015"
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// A variable that holds how many students are registered in the course.
        /// Example: 13
        /// </summary>
        public int StudentCount { get; set; }
    }
}
